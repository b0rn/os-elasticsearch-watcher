#!/bin/bash

echo "Enter the service's name (defaults to os-elasticsearch-watcher) :"
read service_name

echo "Enter the path to project folder (defaults to $(pwd)) :"
read path

echo "Enter the username who will execute the script :"
read username

if [ -z "$service_name" ]; then service_name="os-elasticsearch-watcher"; fi
if [ -z "$path" ]; then path=$(pwd); fi
if [ -z "$username" ]; then 
    echo "Username can't be empty."
    echo "Exiting." 
    exit 1
fi

if [[ ! -f "$path/watcher.py" ]] || [[ ! -f "$path/watcher.yaml" ]]; then
    echo "Missing files watcher.py and/or watcher.yaml in directory $path ."
    echo "Exiting."
    exit 1
fi

if [ -f "/etc/systemd/system/$service_name.service" ]; then
    echo "File /etc/systemd/system/$service_name.service already exists."
    read -p "Overwrite ? (y/n) " -n 1 -r
    echo ""
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        echo "Exiting."
        exit 0
    fi
fi

cat > "/etc/systemd/system/$service_name.service" <<- EOM
[Unit]
Description=Open Source Elasticsearch watcher service
After=network.target
[Service]
Type=simple
Restart=always
RestartSec=3
User=$username
ExecStart=/usr/bin/python3 $path/watcher.py $path

[Install]
WantedBy=multi-user.target
EOM

read -p "Enable the service (automatically start on boot) ? (y/n) " -n 1 -r
echo ""
if [[  $REPLY =~ ^[Yy]$ ]]; then systemctl enable $service_name; fi

read -p "Start the service ? (y/n) " -n 1 -r
echo ""
if [[  $REPLY =~ ^[Yy]$ ]]; then systemctl start $service_name; fi