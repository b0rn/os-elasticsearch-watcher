#!/usr/bin/python3
import logging
import sys
import signal
import os
from config import Config

basePath = os.path.dirname(os.path.realpath(__file__)) if len(sys.argv) < 2 else sys.argv[1]
logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s',filename=basePath + '/log.log',filemode='w', level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())
config_file = basePath +"/watcher.yaml"
try:
    logging.info("Loading config from {}...".format(config_file))
    config = Config(config_file)
    config.elastic.init()
except Exception as err:
    logging.error("{}. Shutting down.".format(err))
    sys.exit(1)

logging.info("Starting pipelines : {}...".format(list(config.pipelines.keys())))
for pipeline in config.pipelines.values():
    try:
        pipeline.start()
    except Exception as err:
        logging.warning("Failed to launch pipeline {} : {}".format(pipeline.name,err))

def cleanup(*args):
    for pipeline in config.pipelines.values():
        pipeline.stop()
        pipeline.join()
    for alert_profile in config.alert_profiles.values():
        del alert_profile
    logging.info("Exiting")

signal.signal(signal.SIGINT, cleanup)
signal.signal(signal.SIGTERM, cleanup)