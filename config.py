import yaml
from elasticsearch import Elasticsearch
from ssl import create_default_context
import logging
import alert_profile
import pipeline
import misc

class ElasticsearchConfig:
    def __init__(self,hosts,username,password,tls,cacert,verify_certs,tls_show_warn):
        self.hosts = misc.get_var('hosts',hosts,list,sub_type=str)
        self.username = misc.get_var('username',username,str)
        self.password = misc.get_var('password',password,str)
        self.tls = misc.get_var('tls',tls,bool,optional=True,default=False)
        self.cacert = misc.get_var('cacert',cacert,str,optional=True,default=None)
        self.verify_certs = misc.get_var('verify_certs',verify_certs,bool,optional=True,default=True)
        self.tls_show_warn = misc.get_var('tls_show_warn',tls_show_warn,bool,optional=True,default=True)
        self.es = None
        self.live = False

    def init(self):
        if self.live:
            return
        logging.info("Initiating Elasticsearch connection...")
        self.es = Elasticsearch(
            hosts=self.hosts,
            http_auth=(self.username,self.password),
            use_ssl=self.tls,
            ca_certs=self.cacert,
            verify_certs=self.verify_certs,
            ssl_show_warn=self.tls_show_warn
        )
        if not self.es.ping():
            raise Exception("could not ping elasticsearch at {}:{}".format(self.hosts,self.port))
        self.live = True

class Config:
    def __init__(self,confFile):
        self.elastic = None
        self.alert_profiles = {}
        self.pipelines = {}

        configContents = {}
        try:
            with open(confFile, 'r') as stream:
                configContents = yaml.safe_load(stream)
        except FileNotFoundError:
            raise Exception("Config file '{}' was not found".format(confFile))
        except Exception as e:
            raise Exception("Failed to read config file {} : {}".format(confFile,e))

        if configContents:
            # BUILD ELASTIC Config
            if "elasticsearch" not in configContents or not isinstance(configContents["elasticsearch"],dict):
                raise Exception("Elasticsearch configuration could not be found")
            
            elastic_config = configContents["elasticsearch"] 
            try:
                self.elastic = ElasticsearchConfig(elastic_config.get("hosts"),elastic_config.get("username"),elastic_config.get("password"),elastic_config.get("tls"),elastic_config.get("cacert"),elastic_config.get("verify_certs"),elastic_config.get("tls_show_warn"))
            except Exception as err:
                raise Exception("Elasticsearch configuration failed : {}".format(err))

            # BUILD ALERT PROFILES
            if "alert_profiles" in configContents and isinstance(configContents["alert_profiles"],dict):
                for name in configContents["alert_profiles"]:
                    profile = configContents["alert_profiles"][name]
                    try:
                        if name in self.alert_profiles:
                            raise Exception("alert profile already exists")
                        alert_type = misc.get_var('type',profile.get('type'),str)
                        if alert_type == "email":
                            self.alert_profiles[name] = alert_profile.MailAlertProfile(name,profile.get("server"),profile.get("port"),profile.get("login"),profile.get("password"),profile.get("tls"),profile.get("cacert"))
                        elif alert_type == "webhook":
                            self.alert_profiles[name] = alert_profile.WebhookAlertProfile(name,profile.get("host"),profile.get("tls"),profile.get("cacert"),profile.get("verify_certs"),profile.get("headers"))
                        elif alert_type == "elasticsearch":
                            if profile.get("use_default") and profile.get("use_default") == True:
                                elasticsearchConfig = self.elastic
                            else:
                                elasticsearchConfig = ElasticsearchConfig(profile.get("hosts"),profile.get("username"),profile.get("password"),profile.get("tls"),profile.get("cacert"),profile.get("verify_certs"),profile.get("tls_show_warn"))
                            self.alert_profiles[name] = alert_profile.ElasticsearchAlertProfile(name,elasticsearchConfig)
                        else:
                            raise Exception("property 'type' must be in the following values : email, webhook, elasticsearch")
                    except Exception as err:
                        logging.warning("Failed to parse '{}' alert profile : {}".format(name,err))
            
            # BUILD PIPELINES
            if "pipelines" in configContents and isinstance(configContents["pipelines"],dict):
                for name in configContents["pipelines"]:
                    p = configContents["pipelines"][name]
                    try:
                        if name in self.pipelines:
                            raise Exception("pipeline already exists")
                        self.pipelines[name] = pipeline.Pipeline(name,p.get('frequency'),p.get('indices'),p.get('query'),p.get('script'),p.get('actions'),self)
                    except Exception as err:
                        logging.warning("Failed to parse '{}' pipeline : {}".format(name,err))