# Open Source Elasticsearch Watcher
Open Source Elasticsearch Watcher is a Open Source alternative to the **Watcher** feature of Elasticsearch (available in Gold version and beyond). It is coded in python and can easily be used as a systemd service.

It consists of pipelines that are scheduled to run an elasticsearch query, then evaluate conditions and perform the specified actions when the conditions are met. For now, the supported actions are the following:
- Elasticsearch indexing
- Email
- Webhook

The pipelines and actions are to be configured in the watcher.yaml file.

## Requirements
- python >= 3.6.x
- PyYAML==5.4.1
- elasticsearch==7.13.2
- requests==2.25.1

## Install and usage
First you need to install the Required python modules : 
```
pip3 install -r requirements.txt
```
Now, if you wish, you can setup the systemd service :
```
bash ./createService.sh
```
You shouldn't start the service right as you did not yet configure it.

## Systemd service
You can create a systemd service with the bash script `createService.sh` :

```
sudo bash ./createService.sh
```

## Configuration
The configuration file consists of three sections :
- elasticsearch : the main elasticsearch configuration
- alert_profiles : the configuration of each alert profile
- pipelines : the configuration of each pipeline


### Elasticsearch
```yaml
elasticsearch:
    # ype : list of strings
    # Description : list of elasticsearch hosts
    hosts:
        - 192.168.12.34:9200
        - my-elk-host:9200

    # Type : string
    # Description : username to use with elasticsearch
    username: username
    
    # Type : string
    # Description : password to use with elasticsearch
    password: password
    
    # Type : boolean
    # Required : no
    # Default : false
    # Description : whether to use tls or not
    tls: true
    
    # Type : string
    # Required : no
    # Description : absolute path to certificate to trust
    cacert: /abs/path/to/cert.pem
    
    # Type : boolean
    # Required : no
    # Default : True
    # Description : whether to verify the certificates or not
    verify_certs: false
    
    # Type : boolean
    # Required : no
    # Default : true
    # Description : whether to show the tls/ssl warnings or not
    tls_show_warn: true
```

### Alert profiles
```yaml
alert_profiles:
    # You can have as many alert profiles as you want but each must have a unique name.

    email_alert_profile:
        # Type : string
        # Required : yes
        # Allowed values : email, webhook, elasticsearch
        # Description : alert profile Type
        Type: email
        
        # Type : string
        # Required : yes
        # Description : smtp host
        server: my.server.net
        
        # Type : int
        # Required : yes
        # Description : smtp port
        port : 25
        
        # Type : string
        # Required : no
        # Description : username to authenticate with
        login: username
        
        # Type : string
        # Required : no
        # Description : password to authenticate with
        password: password
        
        # Type : boolean
        # Required : no
        # Default : false
        # Description : whether to use tls or not
        tls: true
        
        # Type : string
        # Required : no
        # Description : absolute path to certificate to trust
        cacert: /abs/path/to/cert.pem
    
    webhook_alert_profile:
        # Type : string
        # Required : yes
        # Allowed values : email, webhook, elasticsearch
        # Description : alert profile Type
        Type: email
        
        # Type : string
        # Required : yes
        # Description : host to reach
        host : myhost.com
        
        # Type : boolean
        # Required : no
        # Default : false
        # Description : whether to use tls or not
        tls : true
        
        # Type : boolean
        # Required : no
        # Default : True
        # Description : whether to verify the certificates or not
        verify_certs: false
        
        # Type : string
        # Required : no
        # Description : absolute path to certificate to trust
        cacert: /abs/path/to/cert.pem
        
        # Type : dict of strings
        # Description : Default headers to send, dynamic values can be used
        headers:
            header1: 'value'
            header2: '%{foo} dynamic value'
    
    elasticsearch_alert_profile:
        # Type : string
        # Required : yes
        # Allowed values : email, webhook, elasticsearch
        # Description : alert profile Type
        Type: elasticsearch

        # Type : bool
        # Required : no
        # Default : false
        # Description : whether to use the Default elasticsearch configuration or not
        use_default: true

        # This alert profile follows the same schema as the Default elasticsearch configuration
```

### Pipelines
```yaml
pipelines:
    # You can have as many alert profiles as you want but each must have a unique name.

    pipeline_1:
        # Type: string
        # Required : yes
        # Description : The frequency of execution for this pipeline. This string must validate this regex : ^(\d+d)?(\d+h)?(\d+m)?(\d+s)?$ . Examples : 1d, 1d20m, 2d34m45s, 24m, 25m23s, 45s.
        frequency: 2d23m45s

        # Type : list of strings
        # Required : yes
        # Description : the indexes to query
        indices:
            - index1
            - index2-*
        
        # Type : string
        # Required : yes
        # Description : An absolute path to the query file (JSON file).
        query: /absolute/path/to/query.json
        
        # Type : string
        # Required : yes
        # Description : The python script to evaluate after receiving the results to the query. Continue reading the documentation for more information about the script and how to write it.
        script : |
            # Your python script
        
        # The actions to perform when the conditions from the script are met.
        actions:
            # Type : string
            # Required : yes
            # Description : The alert profile id.
            - profile: email_alert_profile
              # These are the fields to use for an email alert profile.

              # Type : string
              # Required : yes
              # Description : The email address sending the email.
              sender: email0@domain.com

              # Type : list of strings
              # Required : yes
              # Description : The email addresses to send the email to.
              to:
                - email1@domain.com
                - email2@domain.com
            
              # Type : string
              # Required : yes
              # Description : The subject of the email. Dynamic values can be used.
              subject: Email subject

              # Type : string
              # Required : yes
              # Description : The body of the email. Dynamic values can be used.
              body: Email body
            
            - profile: webhook_alert_profile
              # These are the fields to use for a webhook alert profile.

              # Type : string
              # Required : yes
              # Description : The path to hit. Must begin with a '/'. Dynamic values can be used.
              path: /endpoint
              
              # Type : string
              # Required : yes
              # Allowed values : GET, POST, PUT, DELETE
              # Description : The HTTP method to use.
              method: POST
              
              # Type : dict of strings
              # Required : no
              # Description : Headers to send. If some of these headers already exist in the alert profile, they will be overwritten. Dynamic values can be used.
              headers:
                header1: "foo"
                header2: "bar"
            
              # Type : string
              # Required : no
              # Description : The encoding to use. Can either be 'form-encoded' or 'json'. Defaults to 'json'.
              body_enc: form-encoded

              # Type : dict
              # Required : no
              # Description : Body to send (only used for POST, PUT and DELETE). Dynamic values can be used but only on the first level of the dictionnary. If a string is supplied, a dynamic value holding a dictionnary is mandatory.
              body:
                prop1: 3
                prop2:
                    - true
                    - false
                prop3 : '%{foo}'
            
            - profile: elasticsearch_alert_profile
              # These are the fields to use for an elasticsearch alert profile.

              # Type : list of string
              # Required : yes
              # Description : The indexes where where the documents will be added.
              indices:
                - index1
                - index3
            
              # Type : string
              # Required : no
              # Description : The id to use to index. Dynamic values can be used.
              id: '%{computed_id}'
              
              # Type : dict or string
              # Required : yes
              # Description : Document to index. Dynamic values can be used but only on the first level of the dictionnary. If a string is supplied, a dynamic value holding a dictionnary is mandatory.
              body:
                prop1: '%{bar}'
                prop2: true
                prop3:
                    a: 34
```

### Script
The script defined in `pipelines.<pipeline_name>.script` controls whether the actions will be executed or not. 

Two variables are already defined in the script : `trigger` and `res`. `res` holds the query's result and `trigger` holds a boolean value telling the pipeline if it has to execute the actions. Setting `trigger` to `True` will make the pipeline to execute the actions.

You can also define new variables in this script and use them in the configuration file via **dynamic values**.

### Dynamic values
Some fields in the configuration file can be used to parse dynamic values (see below). A dynamic value is a variable name, typed like this : `%{var}`, which will be replaced with the variable's value. The variable must be defined in the condition script.
For exemple, if `foo` holds the value `8`, then `Foo value is %{foo}` will be parsed as `Foo value is 8`.