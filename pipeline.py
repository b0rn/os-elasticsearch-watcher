from threading import Thread, Event
import json
import re
import logging

import misc

class Pipeline(Thread):
    __regex_frequency_pattern__ = re.compile(r"^(\d+d)?(\d+h)?(\d+m)?(\d+s)?$")

    def __init__(self,name,frequency,indices,query,script,actions,config):
        Thread.__init__(self,name=name)
        self.name = misc.get_var('name',name,str)
        self.processFrequency(misc.get_var('frequency',frequency,str))
        self.indices = misc.get_var('indices',indices,list,sub_type=str)
        misc.get_var('query',query,str)
        misc.get_var('actions',actions,list)
        self.script = misc.get_var('script',script,str)
        self.config = config

        if Pipeline.__regex_frequency_pattern__.match(frequency) is None:
            raise Exception("property 'frequency' does not valid the frequency format")

        parsed_actions = []
        for i,action in enumerate(actions):
            profile_name = action.get("profile")
            if not profile_name or not isinstance(profile_name,str):
                raise Exception("missing property 'profile' of type str (action n°{})".format(i))
            
            profile = config.alert_profiles.get(profile_name)
            if not profile:
                raise Exception("alert profile '{}' does not exist".format(profile_name))
            
            parsed_actions += [profile.buildAction(action)]
        
        if len(parsed_actions) == 0:
            raise Exception("no actions, skipping")
        
        query_contents = None
        try:
            with open(query,'r') as file:
                contents = file.read()
                query_contents = json.loads(contents)
        except FileNotFoundError:
            raise Exception("query file '{}' not found".format(query))
        
        self.query = query_contents
        self.actions = parsed_actions
        self.event = Event()

    def run(self):
        string = "Pipeline {} will execute every".format(self.name)
        string += " {} day(s)".format(self.frequency[0]) if self.frequency[0] else ""
        string += " {} hour(s)".format(self.frequency[1]) if self.frequency[1] else ""
        string += " {} minute(s)".format(self.frequency[2]) if self.frequency[2] else ""
        string += " {} second(s)".format(self.frequency[3]) if self.frequency[3] else ""
        logging.info(string)

        def __do__(pipeline_name,es,indices,query,script,actions):
            # 1. launch elasticsearch request
            res = es.search(index=','.join(indices),body=query)
            # 2. evaluate conditions
            trigger = False
            local_exec = {"res":res,"trigger":trigger}
            exec(script,{},local_exec)
            trigger = local_exec["trigger"]
            # 3. trigger all actions
            if trigger:
                for action in actions:
                    try:
                        action.do(local_exec)
                    except Exception as e:
                        logging.warning("Pipeline '{}' failed to process action on alert profile '{}' : {}".format(pipeline_name,action.alert_profile.name,e))

        while not self.event.is_set():
            logging.info("Starting execution of pipeline {}".format(self.name))
            Thread(target=__do__,args=[self.name,self.config.elastic.es,self.indices,self.query,self.script,self.actions]).start()
            self.event.wait(self.frequency[-1])
    
    def processFrequency(self,frequency):
        r = Pipeline.__regex_frequency_pattern__.search(frequency)
        if r is None:
            raise Exception("{} is an invalid frequency".format(frequency))
        days = int(r.group(1)[:-1]) if r.group(1) else 0
        hours = int(r.group(2)[:-1]) if r.group(2) else 0
        minutes = int(r.group(3)[:-1]) if r.group(3) else 0
        seconds = int(r.group(4)[:-1]) if r.group(4) else 0
        self.frequency = [days,hours,minutes,seconds,days * 24 * 3600 + hours * 3600 + minutes * 60 + seconds]
    
    def stop(self):
        self.event.set()