class BadPropertyError(Exception):
    def __init__(self,name,t,values=None,optional=True,default=None,suffix_msg="",sub_type=None):
        m = "optional" if optional else "required"
        m += " property '{}' must be of type {}".format(name,t)
        m += " with {} values".format(sub_type) if sub_type else ""
        if values:
            m += " and be in {}".format(values)
        m += " {}".format(suffix_msg)
        if default:
            m += " (defaults to '{}')".format(default)
        super().__init__(m)

def get_var(name,v,t,values=None,optional=False,default=None,suffix_msg="",sub_type=None):
    def check_type(v,t):
        return any(isinstance(v,x) for x in t) if isinstance(t,list) or isinstance(t,tuple) else isinstance(v,t)

    if v is not None:
        if not check_type(v,t) or (values and v not in values):
            raise BadPropertyError(name,t,values,optional,default,suffix_msg,sub_type)
        if sub_type:
            if isinstance(v,list) and not all(check_type(x,sub_type) for x in v):
                raise BadPropertyError(name,t,values,optional,default,suffix_msg,sub_type)
            elif isinstance(v,dict) and not all(check_type(x,sub_type) for x in v.values()):
                raise BadPropertyError(name,t,values,optional,default,suffix_msg,sub_type)
        return v
    else:
        if not optional:
            raise BadPropertyError(name,t,values,optional,default,suffix_msg,sub_type)
        else:
            return default