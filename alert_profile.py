import re
import smtplib
from ssl import create_default_context
import requests
import misc

class AlertProfile:
    def __init__(self,name):
        if not name or not isinstance(name,str):
            raise Exception("missing property 'name' of type str")
        self.name = name
    
    def cleanup(self):
        pass

    def buildAction(self,action):
        pass

class Action:
    __regex_var_pattern__ = re.compile(r"%{([a-zA-Z][a-zA-Z0-9_]*)}+")
    __regex_only_var_pattern = re.compile(r"^%{([a-zA-Z][a-zA-Z0-9_]*)}{1}$")
    def __init__(self,alert_profile):
        if not alert_profile or not isinstance(alert_profile,AlertProfile):
            raise Exception("missing AlertProfile on Action")
        self.alert_profile = alert_profile
    
    def do(self,local_exec):
        pass
    def __replace_var__(text,local_exec):
        m = Action.__regex_only_var_pattern.search(text)
        if m:
            if m.group(1) not in local_exec:
                raise Exception("undefined variable {}".format(m.group(1)))
            return local_exec[m.group(1)]

        m = Action.__regex_var_pattern__.findall(text)
        t = text
        for v_name in m:
            if v_name not in local_exec:
                raise Exception("undefined variable {}".format(v_name))
            t = t.replace('%{'+v_name+'}',str(local_exec[v_name]))
        return t

class MailAlertProfile(AlertProfile):
    def __init__(self,name,server,port,login,password,tls,cacert):
        super().__init__(name)
        self.server = misc.get_var('server',server,str)
        self.port = misc.get_var('port',port,int)
        if not (port > 0 and port <= 65535):
            raise Exception("property 'port' must be an integer in the following range : ]0,65535]")
        misc.get_var('login',login,str,optional=True)
        misc.get_var('password',password,str,optional=True)
        misc.get_var('tls',tls,bool,optional=True)
        misc.get_var('cacert',cacert,str,optional=True)
        
        self.smtp = smtplib.SMTP(server,port)
        self.smtp.ehlo()
        if tls:
            context = create_default_context(cafile=cacert) if cacert else create_default_context()
            self.smtp.starttls(context=context)
            self.smtp.ehlo()
        if login and password:
            self.smtp.login(login,password)
    
    def __del__(self):
        self.smtp.quit()

    def buildAction(self,action):
        return MailAction(self,action)

class MailAction(Action):
    __regex_email_pattern__ = re.compile(r"[^@]+@[^@]+\.[^@]+")
    def __init__(self,alert_profile,action):
        super().__init__(alert_profile)
        suffix_msg = "for action on alert profile {}".format(alert_profile.name)
        self.sender = misc.get_var('sender',action.get("sender"),str,suffix_msg=suffix_msg)
        self.to = misc.get_var('to',action.get('to'),list,sub_type=str,suffix_msg=suffix_msg)
        self.subject = misc.get_var('subject',action.get('subject'),str)
        self.body = misc.get_var('body',action.get('body'),str)
        if MailAction.__regex_email_pattern__.match(action.get("sender")) is None:
            raise Exception("property 'sender' must be a valid email address {}".format(suffix_msg))
        if not all(MailAction.__regex_email_pattern__.match(x) is not None for x in action.get("to")):
            raise Exception("property 'to' must be a list of valid email addresses {}".format(suffix_msg))

    def do(self,local_exec):
        subject = Action.__replace_var__(self.subject,local_exec)
        body = Action.__replace_var__(self.body,local_exec)
        message =  "From:{}\nTo:{}\nSubject: {}\n\n{}".format(self.sender,','.join(self.to),subject,body)
        self.alert_profile.smtp.sendmail(self.sender,self.to,message)

class WebhookAlertProfile(AlertProfile):
    def __init__(self,name,host,tls,cacert,verify_certs,headers):
        super().__init__(name)
        self.host = misc.get_var('host',host,str)
        self.tls = misc.get_var('tls',tls,bool,optional=True,default=False)
        self.verify_certs = misc.get_var('verify_certs',verify_certs,bool,optional=True,default=True)
        self.cacert = misc.get_var('cacert',cacert,str,optional=True,default=None)
        self.headers = misc.get_var('headers',headers,dict,optional=True,default={},sub_type=str)
    
    def buildAction(self,action):
        return WebhookAction(self,action)

class WebhookAction(Action):
    def __init__(self,alert_profile,action):
        super().__init__(alert_profile)
        suffix_msg = "for action on alert profile {}".format(alert_profile.name)
        self.path = misc.get_var('path',action.get('path'),str,suffix_msg=suffix_msg)
        self.method = misc.get_var('method',action.get('method'),str,values=['GET','POST','PUT','DELETE'],suffix_msg=suffix_msg)
        self.body_enc = misc.get_var('body_enc',action.get('body_enc'),str,values=["form-encoded","json"],optional=True,default="json",suffix_msg=suffix_msg)
        self.body = misc.get_var('body',action.get('body'),(dict,str),optional=True,default={},suffix_msg=suffix_msg)
        self.headers = misc.get_var('headers',action.get('headers'),dict,sub_type=str,optional=True,default={},suffix_msg=suffix_msg)
        self.headers = {**alert_profile.headers,**self.headers}
        if not action.get('path').startswith('/'):
            raise Exception("property 'path' must begin with '/'")

    def do(self,local_exec):
        url = "https://" if self.alert_profile.tls else "http://"
        url += self.alert_profile.host
        headers = {k : Action.__replace_var__(v,local_exec) for k,v in self.headers.items()}
        path = Action.__replace_var__(self.path,local_exec)
        body = None
        if isinstance(self.body,str):
            body = misc.get_var('body',Action.__replace_var__(self.body,local_exec),dict,suffix_msg="for action on alert profile {}".format(self.alert_profile.name))
        else:
            # Only replace on 1 level
            body = {k : Action.__replace_var__(v,local_exec) if isinstance(v,str) else v for k,v in self.body.items()}
        print("body",body)
        f_body = {"json" : body} if self.body_enc == "json" else {"data" : body}
        r = None
        verify = self.alert_profile.cacert if self.alert_profile.cacert else self.alert_profile.verify_certs
        if self.method == 'GET':
            r = requests.get(url+path,verify=verify,headers=headers)
        elif self.method == 'POST':
            r = requests.post(url+path,verify=verify,headers=headers,**f_body)
        elif self.method == 'PUT':
            r = requests.put(url+path,verify=verify,headers=headers,**f_body)
        elif self.method == 'DELETE':
            r = requests.delete(url+path,verify=verify,headers=headers,**f_body)
        if r.status_code < 200 or r.status_code >= 300:
            string = """
            Action on {} webhook alert profile resulted in non 2xx status code :
            Status code : {}
            Response : {}
            """.format(self.alert_profile.name,r.status_code,r.text)
            raise Exception(string)        

class ElasticsearchAlertProfile(AlertProfile):
    def __init__(self,name,elasticsearchConfig):
        super().__init__(name)
        self.config = elasticsearchConfig
        self.config.init()
        self.es = self.config.es
    
    def buildAction(self,action):
        return ElasticsearchAction(self,action)        

class ElasticsearchAction(Action):
    def __init__(self,alert_profile,action):
        super().__init__(alert_profile)
        suffix_msg = "for action on alert profile {}".format(alert_profile.name)
        self.indices = misc.get_var('indices',action.get('indices'),list,sub_type=str,suffix_msg=suffix_msg)
        self.id = misc.get_var('id',action.get('id'),str,optional=True,default=None)
        self.body = misc.get_var('body',action.get('body'),(dict,str),suffix_msg=suffix_msg)
    
    def do(self,local_exec):
        identifier = {"id" : Action.__replace_var__(self.id,local_exec)} if self.id else {}
        body = None
        if isinstance(self.body,str):
            body = misc.get_var('body',Action.__replace_var__(self.body,local_exec),dict,suffix_msg="for action on alert profile {}".format(self.alert_profile.name))
        else:
            # Only replace on 1 level
            body = {k : Action.__replace_var__(v) if isinstance(v,str) else v for k,v in self.body.items()}

        for index in self.indices:
            self.alert_profile.es.index(index=index,body=body,**identifier)